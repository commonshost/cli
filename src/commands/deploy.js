const { URL } = require('url')
const { join, resolve, relative, dirname } = require('path')
const { createReadStream, readFile, writeFileSync } = require('fs')
const {
  domainToASCII: toASCII,
  domainToUnicode: toUnicode
} = require('url')
const FormData = require('form-data')
const { promisify } = require('util')
const recursiveReaddir = require('recursive-readdir')
const Listr = require('listr')
const { loadingCredentials } = require('../helpers/loadingCredentials')
const throttle = require('lodash.throttle')
const Observable = require('zen-observable')
const chalk = require('chalk')
const { fetch } = require('../helpers/fetch')
const { parseError } = require('../helpers/parseError')
const projectName = require('project-name')
const Ora = require('ora')
const inquirer = require('inquirer')
const inquirerDirectory = require('inquirer-directory')
const prettyBytes = require('pretty-bytes')
const isValidDomain = require('is-valid-domain')
const isDomainName = require('is-domain-name')
const { findFile } = require('@commonshost/configuration')
const { staticDirectories } = require('@commonshost/static-directories')
const directoryExists = require('directory-exists')
const { tilde } = require('../helpers/tilde')
const { getType } = require('mime')

function stdoutClearLine () {
  if ('clearLine' in process.stdout) {
    process.stdout.clearLine()
  }
}

function stdOutMoveCursor (...args) {
  if ('moveCursor' in process.stdout) {
    process.stdout.moveCursor(...args)
  }
}

function safeDomain (unsafe) {
  return toASCII(unsafe).toLowerCase()
}

async function loadFile (filepath) {
  let loaded
  try {
    loaded = require(filepath)
  } catch (error) {
    if (error.code === 'MODULE_NOT_FOUND') {
      throw new Error(`File not found: ${filepath}`)
    } else {
      throw error
    }
  }
  delete require.cache[filepath]
  return typeof loaded === 'function' ? loaded() : loaded
}

module.exports.command = ['deploy']
module.exports.aliases = ['publish', 'serve', 'sync', 'upload']
module.exports.desc = 'Make files available on the service'
module.exports.builder = {
  options: {
    type: 'string',
    describe: 'Server configuration file'
  },
  manifest: {
    type: 'string',
    describe: 'Server Push Manifest file'
  },
  root: {
    type: 'string',
    describe: 'Directory to serve'
  },
  domain: {
    type: 'string',
    describe: 'Site hostname'
  }
}

async function deploy (argv) {
  const cwd = process.cwd()
  const { accessToken, origin } = await loadingCredentials()

  console.log(chalk.dim(`To cancel, press ${chalk.bold('Ctrl+C')}.\n`))

  let root
  if (argv.root !== undefined) {
    root = resolve(cwd, argv.root)
  } else {
    for (const directory of staticDirectories) {
      const rootpath = join(cwd, directory)
      if (await directoryExists(rootpath)) {
        console.log(chalk.dim(
          'Detected static site:',
          chalk.bold(relative(cwd, rootpath))
        ))
        console.log(chalk.dim(
          `To override, use: ${chalk.bold('--root "path"')}\n`
        ))
        root = rootpath
        break
      }
    }

    if (root === undefined) {
      console.log(chalk.dim(
        'Tip: Skip the directory browser using',
        chalk.bold('--root "path"\n')
      ))

      inquirer.registerPrompt('directory', inquirerDirectory)
      const { directory } = await inquirer.prompt({
        name: 'directory',
        type: 'directory',
        message: 'Choose a folder to upload',
        basePath: cwd
      })
      root = resolve(cwd, directory)
      stdoutClearLine()
      stdOutMoveCursor(0, -1)
      stdoutClearLine()
      stdOutMoveCursor(0, -1)
      stdoutClearLine()
      stdOutMoveCursor(0, -1)
      console.log()
    }
  }

  let configurationFilepath
  if (argv.options) {
    configurationFilepath = argv.options
  } else {
    const filepath = await findFile(cwd)
    if (filepath) {
      console.log(chalk.dim(
        'Detected options file:',
        chalk.bold(relative(cwd, filepath))
      ))
      console.log(chalk.dim(
        `To override, use: ${chalk.bold('--options "path"')}\n`
      ))
      configurationFilepath = filepath
    }
  }

  console.log(chalk.bold('Deploying:'))
  console.log()
  console.log(`   Directory: \t${chalk.bold(tilde(root))}`)

  let files
  {
    const spinner = new Ora('Searching files...').start()
    const ignore = [
      join(cwd, 'CNAME'),
      join(cwd, 'package.json'),
      join(cwd, 'package-lock.json')
    ]
    if (configurationFilepath) {
      ignore.push(resolve(cwd, configurationFilepath))
    }
    let size = 0
    try {
      files = await recursiveReaddir(root, [(file, stats) => {
        if (/(^|[/\\])\../.test(file) || ignore.includes(file)) {
          return true
        } else {
          size += stats.size
          return false
        }
      }])
    } catch (error) {
      spinner.fail()
      throw error
    }
    spinner.stop()
    if (files.length === 0 && argv.confirm === undefined) {
      const { confirm } = await inquirer.prompt({
        name: 'confirm',
        type: 'confirm',
        message: 'No files found. Continue?',
        default: true
      })
      if (confirm === false) {
        process.exit()
      }
    }
    const count = new Intl.NumberFormat().format(files.length)
    const total = prettyBytes(size)
    console.log(`   File count:\t${chalk.bold(count)}`)
    console.log(`   Total size:\t${chalk.bold(total)}`)
  }

  let domain
  let saveCname = false
  {
    const cnames = [
      join(root, 'CNAME'),
      join(cwd, 'CNAME')
    ]
    if (argv.domain !== undefined) {
      domain = argv.domain
    } else {
      for (const cname of cnames) {
        try {
          const raw = await promisify(readFile)(cname, 'utf8')
          const firstEntry = raw.trimLeft().split(/\s/, 2).shift()
          if (firstEntry.length === 0) {
            throw new Error(`Invalid CNAME file: ${relative(cwd, cname)}`)
          } else {
            domain = firstEntry
            break
          }
        } catch (error) {
          if (error.code !== 'ENOENT') {
            throw error
          }
        }
      }
    }
  }

  if (domain === undefined) {
    const url = `${origin}/v2/domains/suggestions` +
      `?project=${encodeURIComponent(projectName(process.cwd()))}`
    const headers = { authorization: `Bearer ${accessToken}` }
    let suggestions
    const spinner = new Ora('Checking available domain names...').start()
    try {
      const response = await fetch(url, { headers })
      if (response.ok) {
        const domains = await response.json()
        suggestions = domains.map(toUnicode)
      } else {
        throw await parseError(response)
      }
    } catch (error) {
      spinner.fail()
      throw error
    }
    spinner.stop()
    const answers = await inquirer.prompt([
      {
        message: 'Choose a domain name:',
        name: 'suggestion',
        type: 'list',
        choices: [
          ...suggestions,
          new inquirer.Separator(),
          {
            name: 'Enter a custom domain...',
            value: 'custom'
          },
          'Cancel'
        ]
      },
      {
        when: ({ suggestion }) => {
          if (suggestion === 'custom') {
            stdoutClearLine()
            stdOutMoveCursor(0, -1)
            stdoutClearLine()
          }
          return suggestion === 'custom'
        },
        message: 'Enter a custom domain name:',
        name: 'custom',
        validate: async (input) => {
          const url = `${origin}/v2/domains/available` +
            `?domain=${encodeURIComponent(input)}`
          try {
            const response = await fetch(url, { headers })
            const GONE = 410
            if (response.status === GONE) {
              return 'Domain unavailable. ' +
                'Try something else, or press Ctrl+C to cancel.'
            }
          } catch (error) {}
          return true
        }
      }
    ])
    if (answers.suggestion === 'custom') {
      domain = answers.custom
      stdoutClearLine()
      stdOutMoveCursor(0, -1)
      stdoutClearLine()
    } else if (answers.suggestion === 'Cancel') {
      process.exit()
    } else {
      stdoutClearLine()
      stdOutMoveCursor(0, -1)
      stdoutClearLine()
      domain = answers.suggestion
    }
    saveCname = true
  }

  if (/^https?:\/\/.+/.test(domain)) {
    try {
      domain = new URL(domain).hostname
    } catch (error) {
      if (error.code !== 'ERR_INVALID_URL') {
        throw error
      }
    }
  }

  domain = safeDomain(domain)
  if (!isValidDomain(domain) || !isDomainName(domain)) {
    throw new Error('Invalid domain name')
  }

  console.log(`   URL:\t\t${chalk.underline.green(`https://${toUnicode(domain)}`)}`)

  let configuration
  let manifestFilepath
  if (configurationFilepath) {
    configuration = await loadFile(resolve(cwd, configurationFilepath))

    if (typeof configuration !== 'object') {
      throw new TypeError('Configuration is not an object')
    } else if (Object.keys(configuration).length === 0) {
      throw new TypeError('Configuration is empty')
    }

    if (Array.isArray(configuration.hosts)) {
      configuration =
        configuration.hosts
          .filter((host) => 'domain' in host)
          .find((host) => domain === safeDomain(host.domain)) ||
        configuration.hosts[0] ||
        {}
    } else {
      configuration = {}
    }

    if (typeof configuration.manifest === 'string') {
      manifestFilepath = resolve(
        dirname(resolve(cwd, configurationFilepath)),
        configuration.manifest
      )
      configuration.manifest = await loadFile(manifestFilepath)
    }
  }

  if (configuration) {
    const path = tilde(resolve(configurationFilepath))
    console.log(`   Options:\t${chalk.bold(path)}`)
  }

  if (!configuration) {
    const guessManifest = resolve(cwd, 'serverpush.json')
    let manifest
    try {
      manifest = await loadFile(guessManifest)
    } catch (error) {
    }
    if (manifest) {
      manifestFilepath = guessManifest
    }
    configuration = { manifest }
  }

  if (argv.manifest) {
    manifestFilepath = resolve(cwd, argv.manifest)
    configuration.manifest = await loadFile(manifestFilepath)
  }

  if (manifestFilepath) {
    const path = tilde(resolve(manifestFilepath))
    console.log(`   Manifest:\t${chalk.bold(path)}`)
  }

  console.log()

  if (argv.confirm === undefined) {
    const { confirm } = await inquirer.prompt({
      name: 'confirm',
      type: 'confirm',
      message: 'Do you want to deploy?',
      default: true
    })
    if (confirm === false) {
      process.exit()
    }
    stdoutClearLine()
    stdOutMoveCursor(0, -1)
  }

  stdoutClearLine()

  await new Listr([{
    title: 'Uploading',
    task: () => new Observable((observer) => {
      const total = files.length
      let pending = total
      const showProgress = throttle(observer.next.bind(observer), 1000 / 10)

      async function onEnd () {
        this.removeListener('error', onError)
        pending -= 1
        if (pending === 0) {
          showProgress('🚀')
        } else {
          const progress = Math.ceil(100 * (total - pending) / total)
          const percentage = `${String(progress).padStart(2)}%`
          const filename = relative(root, this.path)
          showProgress(`${percentage} ${filename}`)
        }
      }

      function onError (error) {
        this.removeListener('end', onEnd)
        observer.error(error)
      }

      const form = new FormData()

      if (configuration !== undefined) {
        form.append('configuration', JSON.stringify(configuration), {
          contentType: 'application/json'
        })
      }

      for (const file of files) {
        const readable = (next) => {
          const stream = createReadStream(file)
          stream.once('error', onError)
          stream.once('end', onEnd)
          next(stream)
        }
        readable.path = file
        const filepath = relative(root, file)
        form.append('directory', readable, {
          filepath,
          contentType: getType(file) || 'application/octet-stream'
        })
      }

      const url = `${origin}/v2/sites/${toASCII(domain)}`
      const fetchOptions = {
        method: 'PUT',
        body: form,
        headers: { authorization: `Bearer ${accessToken}` }
      }
      fetch(url, fetchOptions)
        .then(async (response) => {
          if (response.ok) {
            const data = await response.json()
            if (data.type === 'site-deploy' && data.domain) {
              observer.complete()
            } else {
              observer.error(new Error('Unexpected response'))
            }
          } else {
            observer.error(await parseError(response))
          }
        })
        .catch((error) => {
          observer.error(error)
        })
    })
  }])
    .run()
    .then(() => {
      const idn = toUnicode(domain)
      console.log()
      console.log(chalk.green('Deployment successful!'))
      console.log()

      if (saveCname === true) {
        try {
          writeFileSync(join(cwd, 'CNAME'), idn, { flag: 'wx' })
          console.log(chalk.dim(
            `Domain name saved in file: ${chalk.bold('./CNAME')}\n`
          ))
        } catch (error) {
        }
      }
    })
}

module.exports.handler = async function handler (argv) {
  try {
    await deploy(argv)
  } catch (error) {
    console.error()
    console.error(chalk.bold.red(error.toString()))
    console.error(error.stack.replace(error.toString(), ''))
    console.error()
    process.exit(1)
  }
}
