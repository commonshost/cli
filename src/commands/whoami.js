const { read } = require('../options')

module.exports.command = ['whoami']
module.exports.aliases = []
module.exports.desc = 'Show user information'
module.exports.builder = {
}

module.exports.handler = async function handler (argv) {
  const { username } = await read()
  if (username === undefined) return
  console.log(username)
}
