const createError = require('http-errors')

module.exports.parseError = async (response) => {
  const text = await response.text()

  let data
  try {
    data = JSON.parse(text)
  } catch (error) {
    throw new Error(text)
  }

  if (data.statusCode) {
    const message = data.message ||
      data.error ||
      'Mysterious Error: Report to /r/softwaregore post-haste!'
    throw createError(data.statusCode, message)
  }

  throw new Error(data.message || text)
}
