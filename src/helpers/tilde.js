const { homedir } = require('os')

module.exports.tilde = function tilde (path) {
  const home = homedir()
  if (path.startsWith(home)) {
    return '~' + path.substring(home.length)
  } else {
    return path
  }
}
