const { join } = require('path')
const {
  promises: {
    readFile,
    writeFile,
    mkdir
  }
} = require('fs')
const userHome = require('user-home')

const dirpath = join(userHome, `.${process.title}`)
const filepath = join(dirpath, 'options.json')

module.exports.filepath = filepath

module.exports.read = async function read () {
  try {
    const raw = await readFile(filepath, 'utf8')
    return JSON.parse(raw)
  } catch (error) {
    return {}
  }
}

module.exports.write = async function write (data) {
  const raw = data ? JSON.stringify(data, null, '  ') : ''
  await mkdir(dirpath, { recursive: true })
  await writeFile(filepath, raw, 'utf8')
}
