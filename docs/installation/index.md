# Installation

## Global Installation

Registers the command in the system PATH. Convenient for command line use.

```
npm install --global @commonshost/cli
```

## Local Installation

Add the package to your project's `package.json` under `devDependencies`. This is recommended when using [`npm scripts`](https://docs.npmjs.com/misc/scripts). Saving a specific version as a development dependency avoids conflict between multiple projects using incompatible versions of the same CLI tool.

```
npm install --development @commonshost/cli
```

Use `npx` to run the locally installed, project-specific CLI tool version.

```
npx commonshost
```

## Ephemeral Installation

Use `npx` to temporarily install, run, and immediately clean up any trace of the CLI tool.

```
npx @commonshost/cli
```
