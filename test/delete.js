const test = require('blue-tape')
const { prepare } = require('./helpers')
const { run } = require('./helpers/runner')
const { createServer } = require('http')
const { resolve } = require('path')
const { readFileSync } = require('jsonfile')
const { promisify } = require('util')
const { domainToASCII: toASCII } = require('url')

test('delete', async (t) => {
  const server = createServer(async (request, response) => {
    t.is(request.url, `/v2/sites/${toASCII('💩.example.net')}`)
    t.is(request.method, 'DELETE')
    const validToken = 'test/fixtures/valid_token.json'
    const fixture = resolve(process.cwd(), validToken)
    const { access_token: expectedBearer } = readFileSync(fixture)
    t.is(request.headers.authorization, `Bearer ${expectedBearer}`)
    response.writeHead(200)
    response.end()
  })
  await promisify(server.listen.bind(server))(0)

  // JWT signing key: "secret"
  await prepare('valid_token.json', {
    api: { origin: `http://localhost:${server.address().port}` }
  })

  const command = 'delete --domain 💩.example.net --confirm'
  const { stdout, stderr } = await run(command)
  t.false(stderr)
  t.ok(stdout.includes('Deleted: https://💩.example.net'))

  await promisify(server.close).call(server)
})
