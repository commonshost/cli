module.exports = {
  hosts: [
    {
      manifest: [
        {
          get: '/inline',
          push: '/dependencies'
        }
      ]
    }
  ]
}
