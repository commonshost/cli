const { domainToASCII: toASCII } = require('url')
const { createServer } = require('http')
const path = require('path')
const Busboy = require('busboy')
const { readFileSync } = require('jsonfile')
const { promisify } = require('util')

async function backend (t, expected) {
  const server = createServer(async (request, response) => {
    t.is(request.url, `/v2/sites/${toASCII(expected.domain)}`)
    t.is(request.method, 'PUT')
    const validToken = path.join(__dirname, '../fixtures/valid_token.json')
    const fixture = path.resolve(process.cwd(), validToken)
    const { access_token: expectedBearer } = readFileSync(fixture)
    t.is(request.headers.authorization, `Bearer ${expectedBearer}`)
    const busboy = new Busboy({ headers: request.headers, preservePath: true })
    busboy.on('file', (fieldname, file, filename, encoding, mimetype) => {
      t.is(fieldname, 'directory')
      t.ok(expected.directory.some(({ name, type }) => {
        return name === filename && type === mimetype
      }))
      file.resume()
    })
    busboy.on('field', (fieldname, val, fieldnameTruncated, valTruncated) => {
      t.is(fieldname, 'configuration')
      t.is(val, JSON.stringify(expected.configuration))
    })
    busboy.on('finish', () => {
      response.writeHead(200)
      response.end(JSON.stringify({
        type: 'site-deploy',
        domain: toASCII(expected.domain),
        isNewDomain: true,
        hasNewFiles: true,
        hasNewConfiguration: true
      }))
    })
    request.pipe(busboy)
  })

  await promisify(server.listen.bind(server))(0)

  return server
}

module.exports.backend = backend
