const test = require('blue-tape')
const { prepare } = require('./helpers')
const { run } = require('./helpers/runner')

test('login to Auth0 test environment', async (t) => {
  await prepare('auth0_test.json')
  const command = 'login --username tester --password secret'
  const { stdout, stderr } = await run(command)
  t.false(stderr)
  t.true(stdout.includes('Saving token [completed]'))
})
